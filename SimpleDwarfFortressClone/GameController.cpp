#include "GameController.h"

GameController::GameController()
{
	this->choice = 0;
	this->isInMenu = true;
	this->isPlaying = false;
	fileName = "character.txt";
	//View screen;
}

GameController::~GameController()
{
}


void GameController::mainMenu() // draw main menu and ask user for input
{
	//draw menu
	clearConsole();
	std::cout << "\x1B[33mMain MENU\033[0m\t\t" << std::endl;
	std::cout << "0 : Quit" << std::endl;
	std::cout << "1 : Start" << std::endl;
	std::cout << "2 : Save game" << std::endl;
	std::cout << "3 : Load game" << std::endl;
	std::cout << "4 : Character sheet" << std::endl;
	std::cout << std::endl << "Choice : ";
	std::cin >> choice;

	//switch based on user input
	switch (choice)
	{
	case 0 :				// quit game
		isInMenu = false;
		break;
	case 1 :
		startGame();		// start game
		break;
	case 2 :
		saveGame();			// save game
		break;
	case 3:
		loadGame();			// load game
		break;
	case 4 :				//print user stats
		player.printStats();
		break;
	default:
		std::cout << "wrong choice!";
		//std::cout << std::endl << "Choice : ";
		std::cin >> choice;
		mainMenu();

	}
}
void GameController::initializeEnemy(Enemy enemy)
{

	
	/*for (int i = 24; i <= 40; i++)
	{
		for (int j = 62; j <= 80; j++)
			fileData[i][j] = enemy.getSymbol();
	}*/
	fileData[enemy.getXPos()][enemy.getYPos()] = enemy.getSymbol();
	//fileData[98][43] = '%';
	std::cout << "initialized enemy" << enemy.getName()<< " at position:" <<enemy.getXPos()<<" " << enemy.getYPos() << std::endl;
}
void GameController::startGame()
{	
	Enemy enemy("Goblin", 40, 82, 'G');
	//Enemy enemy2("Mage", 10, 10, 'M');
	clearConsole();
	//CharacterController Controller;
	loadFile("level1.txt");
	initializeEnemy(enemy);
	processPlayerInput(' ');
	//clearConsole();

	std::cout << "Movement:" << std::endl;
	std::cout << "W - up  S - down  A - left  D - right" << std::endl;
	std::cout << "Map explanation:" << std::endl;
	std::cout << playerChar <<" - Player" << std::endl;
	std::cout << "# - Wall" << std::endl;
	std::cout << "P - Health poison" << std::endl;
	std::cout << "E - Enemy" << std::endl;
	std::cout << "W - Weapon " << std::endl;
	std::cout << "A - Armor" << std::endl;
	std::cout << "Press any key to continue." << std::endl;
	getChar();

	clearConsole();
	isInMenu = false;
	isPlaying = true;
	while (isPlaying)
	{
		printFile();
		if (isPlayerTurn())
		{
			processPlayerInput(getChar());
		}
		else
		{
			//std::cout << "enemy moved";
			//setIsPlayerTurn(true);
			MoveEnemy(enemy);
			//MoveEnemy(enemy2);
		}
		clearConsole();
		//move enemy
		// check for attack
		//clear screen

	}
	
}

void GameController::MoveEnemy(Enemy enemy)
{
	static int enemyX = enemy.getXPos();
	static int enemyY = enemy.getYPos();

	//fileData[enemy.getXPos()][enemy.getYPos()] = enemy.getSymbol();
	if (!wasEnemyFound) { //Makes sure the function will run only once        
		findEnemy(enemy, enemyX, enemyY);
		//std::cout << enemyX << " " << enemyY;
		//setFileData(enemy, enemyX, enemyY);
		wasEnemyFound = true;
	}
	int input = rand() % 4 + 1;
	if (input == 1)
	{
		if (checkCollision(enemyX, enemyY, 'W')) {
			enemyY--;
			fileData[enemyY][enemyX] = enemy.getSymbol(); //Changes next empty tile to player tile
			fileData[enemyY + 1][enemyX] = emptyTileChar; //Changes previous player tile to empty tile
		}
	}
	else if (input == 2)
	{
		if (checkCollision(enemyX, enemyY, 'S')) {
			enemyY++;
			fileData[enemyY][enemyX] = enemy.getSymbol(); //Changes next empty tile to player tile
			fileData[enemyY - 1][enemyX] = emptyTileChar; //Changes previous player tile to empty tile
		}
	}
	else if (input == 3)
	{
		if (checkCollision(enemyX, enemyY, 'A')) {
					enemyX--;
					fileData[enemyY][enemyX] = enemy.getSymbol(); //Changes next empty tile to player tile
					fileData[enemyY][enemyX + 1] = emptyTileChar; //Changes previous player tile to empty tile
				}
	}
	else if (input == 4)
	{
		if (checkCollision(enemyX, enemyY, 'D')) {
					enemyX++;
					fileData[enemyY][enemyX] = enemy.getSymbol(); //Changes next empty tile to player tile
					fileData[enemyY][enemyX - 1] = emptyTileChar; //Changes previous player tile to empty tile
				}
	}
	//int input = 3;//rand() % 4 + 1;
	//switch (input)
	//{
	//case '1':
	//	if (checkCollision(enemyX, enemyY, 'W')) {
	//		enemyY--;
	//		fileData[enemyY][enemyX] = enemy.getSymbol(); //Changes next empty tile to player tile
	//		fileData[enemyY + 1][enemyX] = emptyTileChar; //Changes previous player tile to empty tile
	//	}
	//	break;
	//case '2':
	//	if (checkCollision(enemyX, enemyY, 'S')) {
	//		enemyY++;
	//		fileData[enemyY][enemyX] = enemy.getSymbol(); //Changes next empty tile to player tile
	//		fileData[enemyY - 1][enemyX] = emptyTileChar; //Changes previous player tile to empty tile
	//	}
	//	break;
	//case '3':
	//	if (checkCollision(enemyX, enemyY, 'A')) {
	//		enemyX--;
	//		fileData[enemyY][enemyX] = enemy.getSymbol(); //Changes next empty tile to player tile
	//		fileData[enemyY][enemyX + 1] = emptyTileChar; //Changes previous player tile to empty tile
	//	}
	//	break;
	//case '4':
	//	if (checkCollision(enemyX, enemyY, 'D')) {
	//		enemyX++;
	//		fileData[enemyY][enemyX] = enemy.getSymbol(); //Changes next empty tile to player tile
	//		fileData[enemyY][enemyX - 1] = emptyTileChar; //Changes previous player tile to empty tile
	//	}
	//	break;
	//case 27: //Escape
	//	isPlaying = false; //Ends game loop
	//	isInMenu = true;
	//	clearConsole();
	//	mainMenu();
	//	//std::cout << "Thank you for playing!" << std::endl;
	//	//std::cout << "Press any key to end the program...";
	//	//getChar();
	//	break;
	//}
	enemy.SetPosition(enemyX, enemyY);
	//setFileData(enemyX, enemyY);
	//setIsPlayerTurn(false);
	std::cout << "enemy moved to :" << enemy.getXPos() << " " << enemy.getYPos();
	setIsPlayerTurn(true);
}

//bool GameController::isPlayerTurn()
//{
//	return isPlayerTurn;
//}
void GameController::saveGame()
{
	std::ofstream outFile(fileName);

	if (outFile.is_open())
	{
		outFile << player.getAsString();
	}
	outFile.close();
	mainMenu();
}

void GameController::loadGame()
{
}

std::string GameController::GetCharacterInfo()
{
	return player.getAsString();
}

void GameController::setFileData(int currentPlayerX, int currentPlayerY) { //Sets data to display
	std::string line;
	fileDataToDisplay.clear();

	for (unsigned int i = 0; i < Y_CONSOLE_SIZE; i++) {
		line.clear();
		for (unsigned int j = 0; j < X_CONSOLE_SIZE; j++) {
			line += fileData[currentPlayerY - (Y_CONSOLE_SIZE / 2 - 1) + i][currentPlayerX - (X_CONSOLE_SIZE / 2 - 1) + j];
		}
		fileDataToDisplay.push_back(line);
	}
}

void GameController::loadFile(std::string filename) { //Loads level from text file
	std::ifstream file;
	file.open(filename);
	if (!(file.fail())) {
		std::string line;
		while (getline(file, line)) {
			fileData.push_back(line);
		}
	}
	else {
		printf("loadFile function failed!\n");
		getChar();
	}
}
char GameController::getChar() { //Gets input from keyboard
	char input;
#ifdef _WIN32 //Windows
	input = _getch();
	if (input == 0 || input == 0xE0) input = _getch(); //In case of input error
#else //Others
	cin >> input;
#endif
	return input;
}

void GameController::printFile() { //Prints file
	for (unsigned int i = 0; i < this->fileDataToDisplay.size(); i++) {
		//if (i == 5 && j == 5)
		//	this->fileDataToDisplay[i][j]
		printf("%s \n", this->fileDataToDisplay[i].c_str());
	}
}

void GameController::processPlayerInput(char input) { //Processes player input
	static int playerX;
	static int playerY;


	if (!wasPlayerFound) { //Makes sure the function will run only once        
		findPlayer(playerX, playerY);
		setFileData(playerX, playerY);
		wasPlayerFound = true;
	}

	switch (input)
	{
	case 'W': //Up
	case 'w':
		if (checkCollision(playerX, playerY, 'W')) {
			playerY--;
			fileData[playerY][playerX] = playerChar; //Changes next empty tile to player tile
			fileData[playerY + 1][playerX] = emptyTileChar; //Changes previous player tile to empty tile
		}
		break;
	case 'S': //Down
	case 's':
		if (checkCollision(playerX, playerY, 'S')) {
			playerY++;
			fileData[playerY][playerX] = playerChar; //Changes next empty tile to player tile
			fileData[playerY - 1][playerX] = emptyTileChar; //Changes previous player tile to empty tile
		}
		break;
	case 'A': //Left
	case 'a':
		if (checkCollision(playerX, playerY, 'A')) {
			playerX--;
			fileData[playerY][playerX] = playerChar; //Changes next empty tile to player tile
			fileData[playerY][playerX + 1] = emptyTileChar; //Changes previous player tile to empty tile
		}
		break;
	case 'D': //Right
	case 'd':
		if (checkCollision(playerX, playerY, 'D')) {
			playerX++;
			fileData[playerY][playerX] = playerChar; //Changes next empty tile to player tile
			fileData[playerY][playerX - 1] = emptyTileChar; //Changes previous player tile to empty tile
		}
		break;
	case 27: //Escape
		isPlaying = false; //Ends game loop
		isInMenu = true;
		clearConsole();
		mainMenu();		
		//std::cout << "Thank you for playing!" << std::endl;
		//std::cout << "Press any key to end the program...";
		//getChar();
		break;
	}
	player.SetPosition(playerX, playerY);
	setFileData(playerX, playerY);
	setIsPlayerTurn(false);
}

void GameController::findPlayer(int &playerX, int &playerY) { //Finds player in fileData vectors
	char tile;
	//Loop through the file data
	for (unsigned int i = 0; i < fileData.size(); i++) { //Loop through each line of fileData vector
		for (unsigned int j = 0; j < fileData[i].size(); j++) { //Loop through each letter in row i
			tile = fileData[i][j]; //This is the current tile we are looking at
			if (tile == '@') { //Checks if player was found
			  // j - x coordinate in file, i - y coordinate in file
			  //First char in file coordinates are: x = 0, y = 0
				playerX = j;
				playerY = i;
				fileData[i][j] = playerChar;
				break;
			}
		}
	}
}
void GameController::findEnemy(Enemy enemy, int &enemyX, int &enemyY) { //Finds enemy in fileData vectors
	char tile;
	//Loop through the file data
	for (unsigned int i = 0; i < fileData.size(); i++) { //Loop through each line of fileData vector
		for (unsigned int j = 0; j < fileData[i].size(); j++) { //Loop through each letter in row i
			tile = fileData[i][j]; //This is the current tile we are looking at
			if (tile == enemy.getSymbol()) { //Checks if enemy was found
			  // j - x coordinate in file, i - y coordinate in file
			  //First char in file coordinates are: x = 0, y = 0
				enemyX = j;
				enemyY = i;
				fileData[i][j] = enemy.getSymbol();
				break;
			}
		}
	}
}

bool GameController::checkCollision(int currentPlayerX, int currentPlayerY, char key) { //Checks collision between player and game objects
	int playerNextX;
	int playerNextY;
	char tile;
	int canEnter;
	switch (key) {
	case 'W': //Up
		playerNextX = currentPlayerX;
		playerNextY = (currentPlayerY - 1);
		break;
	case 'S': //Down
		playerNextX = currentPlayerX;
		playerNextY = (currentPlayerY + 1);
		break;
	case 'A': //Left
		playerNextX = (currentPlayerX - 1);
		playerNextY = currentPlayerY;
		break;
	case 'D': //Right
		playerNextX = (currentPlayerX + 1);
		playerNextY = currentPlayerY;
		break;
	}
	tile = fileData[playerNextY][playerNextX]; //Tile which player wants to enter
	switch (tile) { //Checks what is next tile
		//TODO: check for array of tiles
	case '#': //Wall
		canEnter = false;
		break;
	case ' ': //Empty tile
		canEnter = true;
		break;
	case 'P':
		canEnter = true;
		std::cout << "Health potion drinked!";
		break;
	default: //Unexpected tile/error
		canEnter = false;
		std::cout << "COLLISION DEFAULT";
		break;
	}
	return canEnter;
}

void GameController::clearConsole() { //Clears console
#ifdef _WIN32 //Windows
	system("CLS");
#else //Others
	system("clear");
#endif
}
