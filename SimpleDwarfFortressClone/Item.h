#pragma once
#include<iostream>
#include<iomanip>
#include<string>


class Item
{
public:
	Item(std::string name = "NONE", int level=0, int value = 0, int rarity= 0);
	virtual ~Item();

	inline std::string  debugPrint() const
	{
		return this->name;
	}
	virtual Item* clone()const = 0;
	//Accessors
	inline const std::string& getName() const { return this->name; };
	inline const int& getValue() const { return this->value; };
	inline const int& getRarity() const { return this->rarity; };
	inline const int& getLevel() const { return this->level; };

	//Modifiers


private:
	std::string name;
	int value;
	int rarity;
	int level;

};

