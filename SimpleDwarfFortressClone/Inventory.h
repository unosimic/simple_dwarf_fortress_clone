#pragma once

//#include"Item.h"
#include"Weapon.h"
class Inventory
{

private:
	int capacity;
	int numOfItems;
	Item **itemArray;
	void expand();
	void initialize(const int from);

public:
	Inventory();
	virtual ~Inventory();
	void addItem(const Item &item);
	void removeItem(int index);
	inline void debugPrint() const
	{
		for (size_t i = 0; i < this->numOfItems; i++)
		{
			std::cout << this->itemArray[i]->debugPrint() << std::endl;
		}
	}
};

