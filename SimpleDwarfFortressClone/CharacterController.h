#pragma once
//#include "GameController.h"
#include <vector>
class Game;
class CharacterController
{

public:
	void processPlayerInput(char input);
	void findPlayer(int &playerX, int &playerY);
	bool checkCollision(int currentPlayerX, int currentPlayerY, char key);
	//void setGameController(Game gameController);
	void setFileData(int currentPlayerX, int currentPlayerY);
	

private:
	Game* game;
	const char emptyTileChar = ' ';
	const char playerChar = 1;
	std::vector<std::string> fileData;
	bool wasPlayerFound = false;


};

