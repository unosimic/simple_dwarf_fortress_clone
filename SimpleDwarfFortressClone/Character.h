#pragma once

#include"Inventory.h"
//class Enemy;
class Character
{
public:
	Character(std::string name = "NONE", int xPos = 0, int yPos = 0, char symbol = ' '  );
	~Character();

	//Functions
	void initialize(std::string name);
	void move(int direction);
	void printStats() const;
	void takeDamage(const int damage);
	std::string getAsString() const;

	//Accessors
	inline const double& getXPos() const { return this->xPos; }
	inline const double& getYPos() const { return this->yPos; }
	inline const std::string& getName() const { return this->name; }
	inline const int& getDamage() const { return this->damage; }
	inline const int& getHealth() const { return this->health; }
	inline const int& getMaxHealth() const { return this->maxHealth; }
	inline const int& getSymbol() const { return this->symbol; }

	//Modifiers
	void SetPosition(int xPos, int yPos);


private:
	std::string name;
	int damage;// min max?
	int health;
	int maxHealth;
protected:
	char symbol;
	int xPos;
	int yPos;

};

