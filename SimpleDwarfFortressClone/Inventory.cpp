#include "Inventory.h"


Inventory::Inventory()
{
	this->capacity = 10;
	this->numOfItems = 0;
	this->itemArray = new Item*[capacity];
}

Inventory::~Inventory()
{
	for (size_t i = 0; i < this->numOfItems; i++)
	{
		delete this->itemArray[i];
	}
	delete[] itemArray;
}

void Inventory::initialize(const int from)
{
	for (size_t i = from; i < capacity; i++)
	{
		this->itemArray[i] = nullptr;
	}
}

void Inventory::expand()
{
	this->capacity *= 2;
	Item **tempArray = new Item*[this->capacity];

	for (size_t i = 0; i < this->numOfItems; i++)
	{
		tempArray[i] = this->itemArray[i];
	}

	//for (size_t i = 0; i < this->numOfItems; i++)
	//{
	//	delete this->itemArray[i];
	//}
	delete[] this->itemArray;

	this->itemArray = tempArray;

	this->initialize(this->numOfItems);
}

void Inventory::addItem(const Item & item)
{
	if (this->numOfItems >= this->capacity)
	{
		expand();
	}
	this->itemArray[this->numOfItems++] = item.clone();
}

void Inventory::removeItem(int index)
{
}
