#pragma once
#include "Item.h"
class Armor :
	public Item
{
public:
	Armor(int type = 0, int defence = 0, std::string name = "NONE", int level = 0, int value = 0, int rarity = 0);
	virtual ~Armor();
	//functions
	std::string toString();

	//Pure virtual
	virtual Armor* clone()const;
	//accessors

	//modifiers


private:
	int type;
	int defence;
};

