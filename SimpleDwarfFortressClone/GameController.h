#pragma once

#include<ctime>
#include"Functions.h"
#include"Character.h"
#include"CharacterController.h"
#include "Enemy.h"
//#include"View.h"
#include<fstream>
#include<vector>
#include<conio.h>
#include<stdlib.h>
//class View;
//class Enemy;
class GameController
{
public:
	GameController();
	virtual ~GameController();

	//Operators

	//Functions
	//void callDrawMainMenu(View activeScreen);
	void mainMenu();
	void endGame();
	void startGame();
	void saveGame();
	void loadGame();
	void clearConsole();
	void loadFile(std::string filename);
	void printFile();
	std::string GetCharacterInfo();
	char getChar();
	//void initializeMap(Enemy enemy);//(Enemy* arr[]);
	void initializeEnemy(Enemy enemy);
	void MoveEnemy(Enemy enemy);

	//character
	void processPlayerInput(char input);
	void findPlayer(int &playerX, int &playerY);
	void findEnemy(Enemy enemy, int &EnemyX, int &EnemyY);
	bool checkCollision(int currentPlayerX, int currentPlayerY, char key);
	//void setGameController(Game gameController);
	void setFileData(int currentPlayerX, int currentPlayerY);
	//bool checkForPickup(int currentPlayerX, int currentPlayerY, char key);
	//Accessors
	inline bool getIsInMenu() const { return this->isInMenu; };
	inline bool getIsPlaying() const { return this->isPlaying; };
	inline bool isPlayerTurn() const { return this->PlayerTurn; };
	//Modifiers
	void setIsInMenu(bool isInMenu) { this->isInMenu = isInMenu; };
	void setIsPlaying(bool isPlaying) { this->isPlaying = isPlaying; };
	void setIsPlayerTurn(bool isPlayerTurn) { this->PlayerTurn = isPlayerTurn; };


private:
	int choice;
	bool isInMenu;
	bool isPlaying;
	bool PlayerTurn = true;

	const unsigned short X_CONSOLE_SIZE = 39;//39; //Number must be uneven
	const unsigned short Y_CONSOLE_SIZE = 21;//21; //Number must be uneven

	std::vector<std::string> fileData; //Entire file data
	std::vector<std::string> fileDataToDisplay; //File data to display in console

	Character player;
	std::string fileName;

	//character
	const char emptyTileChar = ' ';
	const char playerChar = 1;
	const char healthPoison = 'P';
	const char weapon = 'W';
	const char armor = 'A';
	bool wasPlayerFound = false;
	bool wasEnemyFound = false;
	
};

