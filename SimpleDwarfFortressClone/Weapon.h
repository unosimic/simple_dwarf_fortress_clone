#pragma once
#include "Item.h"
class Weapon :
	public Item
{
public:
	Weapon(int damageMin = 0, int damageMax = 0, std::string name = "NONE", int level = 0, int value = 0, int rarity = 0);
	virtual ~Weapon();

		//Pure virtual
	virtual Weapon* clone() const;

		//functions
		std::string toString();

private:
	int damageMin;
	int damageMax;


};

