#pragma once
#include "Character.h"
class Enemy :
	public Character
{
public:
	Enemy(std::string name = "NONE", int xPos = 0, int yPos = 0, char symbol = ' ');
	virtual ~Enemy();

	//Functions

	//Accessors
	char getSymbol() const { return symbol; }
	int getXPos() const { return xPos; }
	int getYPos() const { return yPos; }
	//Modifiers
};

