#include "CharacterController.h"


//void CharacterController::processPlayerInput(char input) { //Processes player input
//	static int playerX;
//	static int playerY;
//
//
//	if (!wasPlayerFound) { //Makes sure the function will run only once        
//		findPlayer(playerX, playerY);
//		setFileData(playerX, playerY);
//		wasPlayerFound = true;
//	}
//
//	switch (input)
//	{
//	case 'W': //Up
//	case 'w':
//		if (checkCollision(playerX, playerY, 'W') == 0) {
//			playerY--;
//			fileData[playerY][playerX] = playerChar; //Changes next empty tile to player tile
//			fileData[playerY + 1][playerX] = emptyTileChar; //Changes previous player tile to empty tile
//		}
//		break;
//	case 'S': //Down
//	case 's':
//		if (checkCollision(playerX, playerY, 'S') == 0) {
//			playerY++;
//			fileData[playerY][playerX] = playerChar; //Changes next empty tile to player tile
//			fileData[playerY - 1][playerX] = emptyTileChar; //Changes previous player tile to empty tile
//		}
//		break;
//	case 'A': //Left
//	case 'a':
//		if (checkCollision(playerX, playerY, 'A') == 0) {
//			playerX--;
//			fileData[playerY][playerX] = playerChar; //Changes next empty tile to player tile
//			fileData[playerY][playerX + 1] = emptyTileChar; //Changes previous player tile to empty tile
//		}
//		break;
//	case 'D': //Right
//	case 'd':
//		if (checkCollision(playerX, playerY, 'D') == 0) {
//			playerX++;
//			fileData[playerY][playerX] = playerChar; //Changes next empty tile to player tile
//			fileData[playerY][playerX - 1] = emptyTileChar; //Changes previous player tile to empty tile
//		}
//		break;
//	case 27: //Escape
//		isPlaying = true; //Ends game loop
//		clearConsole();
//		std::cout << "Thank you for playing!" << std::endl;
//		std::cout << "Press any key to end the program...";
//		getChar();
//		break;
//	}
//
//	setFileData(playerX, playerY);
//}
//
//void CharacterController::findPlayer(int &playerX, int &playerY) { //Finds player in fileData vectors
//	char tile;
//	//Loop through the file data
//	for (unsigned int i = 0; i < fileData.size(); i++) { //Loop through each line of fileData vector
//		for (unsigned int j = 0; j < fileData[i].size(); j++) { //Loop through each letter in row i
//			tile = fileData[i][j]; //This is the current tile we are looking at
//			if (tile == '@') { //Checks if player was found
//			  // j - x coordinate in file, i - y coordinate in file
//			  //First char in file coordinates are: x = 0, y = 0
//				playerX = j;
//				playerY = i;
//				fileData[i][j] = playerChar;
//				break;
//			}
//		}
//	}
//}
//
//bool CharacterController::checkCollision(int currentPlayerX, int currentPlayerY, char key) { //Checks collision between player and game objects
//	int playerNextX;
//	int playerNextY;
//	char tile;
//	int canEnter;
//	switch (key) {
//	case 'W': //Up
//		playerNextX = currentPlayerX;
//		playerNextY = (currentPlayerY - 1);
//		break;
//	case 'S': //Down
//		playerNextX = currentPlayerX;
//		playerNextY = (currentPlayerY + 1);
//		break;
//	case 'A': //Left
//		playerNextX = (currentPlayerX - 1);
//		playerNextY = currentPlayerY;
//		break;
//	case 'D': //Right
//		playerNextX = (currentPlayerX + 1);
//		playerNextY = currentPlayerY;
//		break;
//	}
//	tile = fileData[playerNextY][playerNextX]; //Tile which player wants to enter
//	switch (tile) { //Checks what is next tile
//	case '#': //Wall
//		canEnter = false;
//		break;
//	case ' ': //Empty tile
//		canEnter = true;
//		break;
//	default: //Unexpected tile/error
//		canEnter = false;
//		break;
//	}
//	return canEnter;
//}

