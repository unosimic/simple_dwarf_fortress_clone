#include "Armor.h"

Armor::Armor(int type, int defence, std::string name, int level, int value, int rarity)
	: Item(name, level, value, rarity)
{
	this->type = type;
	this->defence = defence;
}

Armor::~Armor()
{

}

std::string Armor::toString()
{
	std::string str = std::to_string(this->type) + " " + std::to_string(this->defence);

	return str;
}

Armor * Armor::clone() const
{
	return new Armor(*this);
}
