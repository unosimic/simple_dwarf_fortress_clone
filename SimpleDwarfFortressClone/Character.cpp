#include "Character.h"

Character::Character(std::string name, int xPos, int yPos, char symbol)
{

	this->xPos = xPos;
	this->yPos = yPos;

	this->name = name;
	this->damage = 40;// min max?
	this->health = 100;
	this->maxHealth = 0;
	this->symbol = symbol;
}

Character::~Character()
{
}

void Character::initialize(std::string name)
{
	this->xPos = 0;
	this->yPos = 0;

	this->name = name;
	this->damage = 40;// min max?
	this->health = 100;
	this->maxHealth = 100;
}

void Character::printStats() const
{
	std::cout << getAsString();
}

std::string Character::getAsString() const
{
	return std::to_string(xPos) + " "
		+ std::to_string(yPos) + " "
		+ name + " "
		+ std::to_string(damage) + " "
		+ std::to_string(health);
}

void move(int direction)
{
	//check if can move
	//if can, move
	//if not do nothing

}

void Character::SetPosition(int xPos, int yPos)
{
	this->xPos = xPos;
	this->yPos = yPos;
}